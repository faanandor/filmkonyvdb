package filmkonyvdb;

import annotation.GetterFunctionName;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Funkciok<Osztaly> {
    public Boolean mentes(Osztaly peldany) {
        String osztalynev = peldany.getClass().getSimpleName();
        String fajlnev = osztalynev + ".xml";
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            TransformerFactory tf = TransformerFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            //Document xml = db.newDocument();
            Transformer t = tf.newTransformer();
            File file = new File(fajlnev);
            if (!(file.exists() && file.isFile())) {
                file.createNewFile();
                Document xml = db.newDocument();
                t.setOutputProperty(OutputKeys.ENCODING, "utf-8");
                String foelemNev = peldany.getClass().getName();
                Element foelem = xml.createElement(foelemNev);
                xml.appendChild(foelem);
                DOMSource source = new DOMSource(xml);
                StreamResult result = new StreamResult(file);
                t.transform(source, result);
            }
            // Mentés Folyamata
            Field[] tulajdonsagok = peldany.getClass().getDeclaredFields();
            HashMap<String, HashMap<String,String>> adatok = new HashMap<String,HashMap<String,String>>();
            for (Field tul : tulajdonsagok) {
                String getterFunctionName = tul.getAnnotation(GetterFunctionName.class).value(); 
                String tulTipus = tul.getAnnotation(GetterFunctionName.class).type().getSimpleName();
                Method getterFuggveny = peldany.getClass().getMethod(getterFunctionName);
                String tulajdonsagErtek = getterFuggveny.invoke(peldany).toString();
                HashMap<String,String> ertekAdatok = new HashMap<String,String>();
                ertekAdatok.put(tulajdonsagErtek, tulTipus);
                adatok.put(tul.getName(), ertekAdatok);
            }
            tulajdonsagok = peldany.getClass().getSuperclass().getDeclaredFields();
            for (Field tul : tulajdonsagok) {
                String getterFunctionName = tul.getAnnotation(GetterFunctionName.class).value(); 
                String tulTipus = tul.getAnnotation(GetterFunctionName.class).type().getSimpleName();
                Method getterFuggveny = peldany.getClass().getSuperclass().getMethod(getterFunctionName);
                String tulajdonsagErtek = getterFuggveny.invoke(peldany).toString();
                HashMap<String,String> ertekAdatok = new HashMap<String,String>();
                ertekAdatok.put(tulajdonsagErtek, tulTipus);
                adatok.put(tul.getName(), ertekAdatok);
            }
            Document xml = db.parse(file);
            xml.normalize();
            Element foelem = (Element)xml.getFirstChild();
            Element ujElem = xml.createElement(peldany.getClass().getSimpleName());
            foelem.appendChild(ujElem);
            for (Map.Entry<String, HashMap<String, String>> adat : adatok.entrySet()) {
                String tulNev = adat.getKey();
                HashMap<String, String> ertekAdatok = adat.getValue();
                String veglegesErtek = "";
                String veglegesTipus = "";
                for(Map.Entry<String, String> ertekAdat : ertekAdatok.entrySet()){
                    veglegesErtek = ertekAdat.getKey();
                    veglegesTipus = ertekAdat.getValue();
                }
                Element tulajdonsag = xml.createElement(tulNev);
                tulajdonsag.setTextContent(veglegesErtek);
                tulajdonsag.setAttribute("type", veglegesTipus);
                ujElem.appendChild(tulajdonsag);
            }
            DOMSource source = new DOMSource(xml);
            StreamResult result = new StreamResult(file);
            t.transform(source, result);
            
            return Boolean.TRUE;
        }
        catch (Exception err) {
            System.out.println(err.toString());
        }
        return Boolean.FALSE;
    }
    // Adatok betöltése a statisztikához
    public void betoltes(Osztaly peldany, LinkedList<String> lista, LinkedList<Integer> listaInt) throws IOException {
        String osztalynev = peldany.getClass().getSimpleName();
        String filePath = peldany.getClass().getSimpleName() + ".xml";
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(osztalynev);
            List<Alkotas> dataList = new ArrayList<Alkotas>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                dataList.add(getAlkotas(nodeList.item(i), lista, listaInt));
            }
        }
        catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }
    }
    // A szükséges adatokat kivesszük
    private static Alkotas getAlkotas(Node node, LinkedList<String> cimList, LinkedList<Integer> profitList) {
        Alkotas alkotas = new Alkotas();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            String cim; int bevetel;
            Element element = (Element) node;
            alkotas.getCim();
            cim = getTagValue("cim", element); cimList.add(cim);
            alkotas.getBevetel();
            bevetel = Integer.parseInt(getTagValue("bevetel", element)); profitList.add(bevetel);
        }
        return alkotas;
    }
    // A film adatok betöltése a táblázathoz
    public void filmBetoltes(Osztaly peldany, LinkedList<Integer> filmHossz, LinkedList<Boolean> filmTopSzaz, LinkedList<String> filmCim, LinkedList<String> filmAlkoto, LinkedList<String> filmMufaj, LinkedList<Integer> filmBevetel) {
        String osztalynev = peldany.getClass().getSimpleName();
        String filePath = peldany.getClass().getSimpleName() + ".xml";
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(osztalynev);
            List<Film> dataList = new ArrayList<Film>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                dataList.add(getFilm(nodeList.item(i), filmHossz, filmTopSzaz, filmCim, filmAlkoto, filmMufaj, filmBevetel));
            }
        }
        catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }
    }
    // A film adatok kinyerése
    private static Film getFilm(Node node, LinkedList<Integer> filmHossz, LinkedList<Boolean> filmTopSzaz, LinkedList<String> filmCim, LinkedList<String> filmAlkoto, LinkedList<String> filmMufaj, LinkedList<Integer> filmBevetel) {
        Film f = new Film();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            int hossz; boolean isTopSzaz; String cim; String alkoto; String mufaj; int bevetel;
            Element element = (Element) node;
            f.getHossz(); hossz = Integer.parseInt(getTagValue("hossz", element)); filmHossz.add(hossz);
            f.isTopSzaz(); isTopSzaz = Boolean.getBoolean(getTagValue("topSzaz", element)); filmTopSzaz.add(isTopSzaz);
            f.getCim(); cim = getTagValue("cim", element); filmCim.add(cim);
            f.getAlkoto(); alkoto = getTagValue("alkoto", element); filmAlkoto.add(alkoto);
            f.getMufaj(); mufaj = getTagValue("mufaj", element); filmMufaj.add(mufaj);
            f.getBevetel(); bevetel = Integer.parseInt(getTagValue("bevetel", element)); filmBevetel.add(bevetel);
        }
        return f;
    }
    
    // A könyv adatok betöltése a táblázathoz
    public void konyvBetoltes(Osztaly peldany, LinkedList<Integer> hossz, LinkedList<Boolean> hosszu, LinkedList<String> cim, LinkedList<String> alkoto, LinkedList<String> mufaj, LinkedList<Integer> bevetel) {
        String osztalynev = peldany.getClass().getSimpleName();
        String filePath = peldany.getClass().getSimpleName() + ".xml";
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();
            NodeList nodeList = doc.getElementsByTagName(osztalynev);
            List<Konyv> dataList = new ArrayList<Konyv>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                dataList.add(getKonyv(nodeList.item(i), hossz, hosszu, cim, alkoto, mufaj, bevetel));
            }
        }
        catch (SAXException | ParserConfigurationException | IOException e1) {
            e1.printStackTrace();
        }
    }
    // A könyv adatok kinyerése
    private static Konyv getKonyv(Node node, LinkedList<Integer> konyvHossz, LinkedList<Boolean> konyvHosszu, LinkedList<String> konyvCim, LinkedList<String> konyvAlkoto, LinkedList<String> konyvMufaj, LinkedList<Integer> konyvBevetel) {
        Konyv k = new Konyv();
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            int hossz; boolean hosszu; String cim; String alkoto; String mufaj; int bevetel;
            Element element = (Element) node;
            k.getHossz(); hossz = Integer.parseInt(getTagValue("hossz", element)); konyvHossz.add(hossz);
            k.isHosszu(); hosszu = Boolean.getBoolean(getTagValue("hosszu", element)); konyvHosszu.add(hosszu);
            k.getCim(); cim = getTagValue("cim", element); konyvCim.add(cim);
            k.getAlkoto(); alkoto = getTagValue("alkoto", element); konyvAlkoto.add(alkoto);
            k.getMufaj(); mufaj = getTagValue("mufaj", element); konyvMufaj.add(mufaj);
            k.getBevetel(); bevetel = Integer.parseInt(getTagValue("bevetel", element)); konyvBevetel.add(bevetel);
        }
        return k;
    }
    
    // Kinyerjük az adatok a függvény segítségével
    private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }
}

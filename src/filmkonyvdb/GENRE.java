package filmkonyvdb;

public enum GENRE {
    ACTION(9, "Nagyon népszerű"), ADVENTURE(10, "Legnépszerűbb"), COMEDY(8, "Népszerű"), DRAMA(8, "Népszerű"), FANTASY(7, "Népszerű"), HORROR(6, "Népszerű egy bizonyos mértékig"), MYSTERY(6, "Népszerű egy bizonyos mértékig"), ROMANCE(5, "Kevésbé népszerű"), SCIFI(5, "Közepes"), THRILLER(7, "Kevésbé népszerű"), WESTERN(3,"Nem túl népszerű");
    // Megjegyzés: A fent látható értékelések internetes vélemények átlaga alapján vannak, nem a saját értékelésem
    
    private Integer mufajSkalaErtek; // Milyen a műfaj értékelése 1-10-es skálán?
    private String kiirandoErtek; // Milyen népszerű a műfaj értékelések alapján
    
    private GENRE (Integer mse, String ke) {
        this.mufajSkalaErtek = mse;
        this.kiirandoErtek = ke;
    }
    
    public Integer getMufajSkalaErtek(){
        return this.mufajSkalaErtek;
    }
    
    public String normalizaltNev() {
        return this.kiirandoErtek;
    }
}

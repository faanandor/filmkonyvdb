package filmkonyvdb;

import annotation.GetterFunctionName;

public class Alkotas {
    @GetterFunctionName(value = "getCim", type = String.class)
    protected String cim;
    @GetterFunctionName(value = "getAlkoto", type = String.class)
    protected String alkoto;
    @GetterFunctionName(value = "getMufaj", type = GENRE.class)
    protected GENRE mufaj;
    @GetterFunctionName(value = "getBevetel", type = Integer.class)
    protected Integer bevetel;
    
    public Alkotas() {
        this.cim = "Ready Player One";
        this.alkoto = "Steven Spielberg";
        this.mufaj = GENRE.SCIFI;
        this.bevetel = 137000000;
    }

    public Alkotas(String cim, String alkoto, GENRE mufaj, Integer bevetel) {
        this.cim = cim;
        this.alkoto = alkoto;
        this.mufaj = mufaj;
        this.bevetel = bevetel;
    }

    public String getCim() {
        return this.cim;
    }

    public String getAlkoto() {
        return this.alkoto;
    }

    public GENRE getMufaj() {
        return this.mufaj;
    }

    public Integer getBevetel() {
        return this.bevetel;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public void setAlkoto(String alkoto) {
        this.alkoto = alkoto;
    }

    public void setMufaj(GENRE mufaj) {
        this.mufaj = mufaj;
    }

    public void setBevetel(Integer bevetel) {
        this.bevetel = bevetel;
    }
    
    
}

package filmkonyvdb;

import annotation.GetterFunctionName;

public class Konyv extends Alkotas {
    @GetterFunctionName(value = "getHossz", type = Integer.class)
    private Integer hossz;
    @GetterFunctionName(value = "isHosszu", type = Boolean.class)
    private Boolean hosszu; // Hosszúnak számít a könyv?
    
    public Konyv() {
        super();
        this.hossz = 90;
        this.hosszu = Boolean.FALSE;
    }

    public Konyv(Integer hossz, Boolean hosszu, String cim, String alkoto, GENRE mufaj, Integer bevetel) {
        super(cim, alkoto, mufaj, bevetel);
        this.hossz = hossz;
        this.hosszu = hosszu;
    }

    public Integer getHossz() {
        return this.hossz;
    }

    public Boolean isHosszu() {
        return this.hosszu;
    }
}

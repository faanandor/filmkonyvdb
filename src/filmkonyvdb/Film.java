package filmkonyvdb;

import annotation.GetterFunctionName;
import annotation.MinMaxValue;
import javax.swing.JOptionPane;
import filmkonyvdb.Ablak;

public class Film extends Alkotas{
    @MinMaxValue(min = 1, max = 1260) // Minimum és maximum érték filmhossznak
    @GetterFunctionName(value = "getHossz", type = Integer.class)
    private Integer hossz; // A film hossza percben (-> átkonvertálás órára?)
    @GetterFunctionName(value = "isTopSzaz", type = Boolean.class)
    private Boolean topSzaz; // Benne van e a top 100 film között IMDB-n
    
    public Film(){
        super();
        this.hossz = 1260;
        this.topSzaz = Boolean.FALSE;
    }

    public Film(Integer hossz, Boolean topSzaz, String cim, String alkoto, GENRE mufaj, Integer bevetel) {
        super(cim, alkoto, mufaj, bevetel);
        if(hossz > 0)this.hossz = hossz;
        else this.hossz = 90;
        this.topSzaz = topSzaz;
    }

    public Integer getHossz() {
        return this.hossz;
    }

    public Boolean isTopSzaz() {
        return this.topSzaz;
    }
    
    public void setHossz(Integer hossz){
        this.hossz = hossz;
    }
    
    public void pluszEgyPerc() {
        try {
            if (this.hossz < Film.class.getDeclaredField("hossz").getAnnotation(MinMaxValue.class).max()) {
                this.hossz++;
            }
        }
        catch (Exception e) {
            System.out.println("Hiba: " + e.toString());
        }
    }
    
    public void pluszTizPerc() {
        try {
            if (this.hossz < Film.class.getDeclaredField("hossz").getAnnotation(MinMaxValue.class).max()) {
                this.hossz += 10;
            }
        }
        catch (Exception e) {
            System.out.println("Hiba: " + e.toString());
        }
    }
    
    public void topSzazLett() {
        this.topSzaz = Boolean.TRUE;
    }
    
    public boolean checkHossz(Integer hossz, Boolean enable) {
        try {
            if (hossz > Film.class.getDeclaredField("hossz").getAnnotation(MinMaxValue.class).max()) {
                JOptionPane.showMessageDialog(null, "A megadott hossz nagyobb a maximumnál!");
                enable = Boolean.FALSE;
            }
            else if (hossz < Film.class.getDeclaredField("hossz").getAnnotation(MinMaxValue.class).min()) {
                JOptionPane.showMessageDialog(null, "A megadott hossz kisebb a minimumnál!");
                enable = Boolean.FALSE;
            }
            else {
                enable = Boolean.TRUE;
            }
        }
        catch (Exception e) {
            System.out.println("Hiba: " + e.toString());
        }
        return enable;
    }
}

package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)  
@Target(ElementType.FIELD) 
public @interface MinMaxValue {
    public int min() default 0;
    public int max() default 1260;
    public int minKonyv() default 1;
    public int maxKonyv() default 10710;
}
